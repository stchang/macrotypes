#lang turnstile

;; turnstile library for specifying type-level reduction rules

(require "more-utils.rkt")

(provide define-red
         define-typerule/red
         reflect/m (for-syntax reflect mk-reflected))

;; macro version of reflect; enables cleaner rules in some cases
;; - doesnt need to be template metafn; waiting until expansion to reflect is ok
(define-syntax reflect/m (syntax-parser [(_ e) (reflect #'e)]))

(begin-for-syntax
  ;; reflects expanded stx to surface, so evaluation may continue
  (define (reflect stx)
    (syntax-parse stx
      [:id stx]
      [(m . rst)
       #:do[(define new-m (syntax-property #'m 'reflect))]
       (transfer-props
        stx
        (datum->syntax stx ; this datum->stx ensures correct #%app inserted
                       (cons (or new-m (reflect #'m)) (stx-map reflect #'rst))
                       stx)
        #:except null)]
      [_ stx]))

  (define (mk-reflected reflected-name placeholder [display-as #f])
    (syntax-property
     (syntax-property
      placeholder
      'reflect
      reflected-name)
     'display-as display-as))
  )

(define-syntax define-red
  (syntax-parser
    [(_ red-name redex (~datum ~>) contractum) ; single redex case
     #'(define-red red-name [redex ~> contractum])]
    ;; NOTE: be careful about "pattern passing" for this macro-defining macro:
    ;; Specifically, if `head-pat` is an id expander and `rst-pat` is null,
    ;; then `head-pat` wont expand to its paren-wrapped nullary-constructor call
    ;; (as defined by define-type in turnstile/eval),
    ;;  bc it will already have parens in the stx-parse clauses below,
    ;; so the match will fail
    ;; eg, see `zero?` in stdlib/nat
    [(_ red-name
        (~optional (~seq #:display-as orig) #:defaults ([orig #'#f]))
        [(placeholder:id . pats) (~datum ~>) contractum] ...)
     #:with placeholder1 (if (stx-null? #'(placeholder ...))
                             #'#%plain-app
                             (stx-car #'(placeholder ...)))
     #:with name- (mk-- #'red-name)
     #'(begin
         (define-core-id name-)
         (define-syntax red-name
           (λ (stx)
             (transfer-type
              stx
              (syntax-parse (stx-map expand/df (stx-cdr stx)) ; drop macro name
                [pats
                 ;; #:do[(displayln 'red-name)
                 ;;      (pretty-print (stx->datum this-syntax))
                 ;;      (displayln '~>)
                 ;;      (pretty-print (stx->datum #`contractum))]
                 (reflect (quasitemplate contractum))] ...
                [es
                 ;; #:do[(display "no match: ") (displayln 'red-name)
                 ;;                             (pretty-print (stx->datum #'es))
                 ;;                             (displayln 'expected)
                 ;;                             (stx-map
                 ;;                              (λ (ps) (pretty-print (stx->datum ps)))
                 ;;                              #'(pats ...))]
                 (quasisyntax/loc stx
                   (#,(mk-reflected #'red-name #'placeholder1 #'orig) . es))])))))]))

;; use #%plain-app for now
(define-syntax define-core-id
  (syntax-parser
    [(_ name:id) #'(define-core-id name #%plain-app)] ; plain-app is the default
    [(_ name x:id)
     #'(define-syntax name (make-rename-transformer #'x))]))

;; combination of define-typerule and define-red
(define-syntax define-typerule/red
  (syntax-parser
    [(_ (name:id . in-pat) (~and rule (~not #:where)) ...
        #:where red-name
        (~optional (~seq #:display-as orig) #:defaults ([orig #'#f]))
        (~and (~not #:display-as) reds) ...)
     #:with name- (mk-- #'name)
     #`(begin-
         #,(quasisyntax/loc this-syntax
             (define-typerule (name . in-pat) rule ...))
         (define-core-id name-) ; a placeholder to use in the red rule
         (define-red red-name #:display-as orig reds ...))]))


